<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Midesign Test Shop</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>

<div class="header">
    
    <a class="logo" href="/">Mindesign Test Shop</a>
    
    <div class="search">
        <form action="{{route('product.search')}}" method="get">
    
        <label>
            Поиск по продуктам:
            <input type="text" class="search__input" name="s"/>
            
        </label>
        <button>Искать</button>
        </form>
    </div>

</div>

@yield('content')

</body>
</html>