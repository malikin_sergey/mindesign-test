@extends('layout')


@section('content')
    
    <div class="layout-layer">
        <div class="layout-block">
            <div class="products-title">Поиск: {{request('s')}}</div>
            
            <div class="products-list products-list--popular">
                
                @foreach($products as $product)
                    
                    
                    <div class="product product--popular">
                        
                        <div class="product__image">
                            <img src="{{$product->image}}" alt="{{$product->title}}">
                        </div>
                        
                        <div class="product__title">{{$product->title}}</div>
                        
                        <div class="product__price">{{$product->price}} руб.</div>
                        
                        <div class="product__sales">Продано: {{$product->sales_sum}} шт.</div>
                        
                        <div class="product__sales">Доступно: {{$product->amount_sum}} шт.</div>
                    
                    </div>
                
                @endforeach
            
            </div>
        </div>
    </div>



@endsection