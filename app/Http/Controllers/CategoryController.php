<?php

namespace App\Http\Controllers;

use App\Category;

/**
 * Class CategoryController
 * @package App\Http\Controllers
 */
class CategoryController extends Controller
{
    /**
     * @param $slug
     */
    public function index($slug)
    {
        $category = Category::where('alias', $slug)->firstOrFail();
        
        $childCategories = $category->Childs;
        
        $products = $category->Products;
        
        return view('category', ['category' => $category, 'childCategories' => $childCategories, 'products' => $products]);
    }
}
