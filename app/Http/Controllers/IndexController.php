<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;

/**
 * Class IndexController
 * @package App\Http\Controllers
 */
class IndexController extends Controller
{
    public function index()
    {
        $popularProducts = Product::query();
        
        $popularProducts = $popularProducts->select('*')
                                           ->addSalesSum()
                                           ->addAmountSum()
                                           ->orderByRaw('sales_sum desc nulls last')
                                           ->take(20)
                                           ->get();
    
        $сategories = Category::whereNull('parent_id')->get();
        
        return view('index', ['popularProducts' => $popularProducts, 'сategories' => $сategories]);
    }
}
