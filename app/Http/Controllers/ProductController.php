<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function search(Request $request)
    {
        $query = $request->s;
        
        $products = Product::select("*")
                           ->whereRaw("to_tsvector('russian', title||' '||description) @@ plainto_tsquery('russian', ?)",
                               [$query]);
    
        //dd($products->toSql());
    
        
        $products = $products->get();
        
        
        return view('search', ['products' => $products]);
    }
}
