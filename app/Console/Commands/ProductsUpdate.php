<?php

namespace App\Console\Commands;

use App\ProductsUpdater;
use Illuminate\Console\Command;

/**
 * Class ProductsUpdate
 * @package App\Console\Commands
 */
class ProductsUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:update';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update products from markethot.ru';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $productsUpdater = new ProductsUpdater();
        
        $productsUpdater->update();
        
        $this->info("Products update done");
    }
}
