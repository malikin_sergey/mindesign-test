<?php

namespace App;

/**
 * Class Category
 * @package App
 */
class Category extends \Eloquent
{
    /**
     * @type string
     */
    protected $table = "categories";
    
    /**
     * @type array
     */
    protected $guarded = ['*'];
    
    /**
     * @type array
     */
    protected $casts = [];
    
    /**
     * @type bool
     */
    public $timestamps = false;
    
    public function Childs()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }
    
    public function Products()
    {
        return $this->belongsToMany(Product::class);
    }
}
