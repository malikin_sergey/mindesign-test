<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 12.09.2018
 * Time: 0:17
 */

namespace App;

/**
 * Class ProductsUpdater
 * @package App
 */
class ProductsUpdater
{
    public function update()
    {
        $data = file_get_contents(config('shop.products_feed_url'));
        
        $data = json_decode($data, true);
        
        \Eloquent::unguard();
        
        /* update products */
        
        foreach ($data['products'] as $productData) {
            
            $product = Product::firstOrNew(['id' => $productData['id']]);
            
            $product->fill(array_only($productData, [
                "title",
                "image",
                "description",
                "first_invoice",
                "url",
                "price",
                "amount",
            ]));
            
            $product->save();
            
            /* update categories */
            
            $categoriesIds = [];
            
            foreach ($productData['categories'] as $categoryData) {
                $category = Category::firstOrNew(['id' => $categoryData['id']]);
                
                $category->fill(array_only($categoryData, [
                    "title",
                    "alias"
                ]));
                
                $category->parent_id = data_get($categoryData, 'parent');
                
                $category->save();
                
                $categoriesIds[] = $category->id;
            }
            
            /* update product categories links */
            
            $product->Categories()->sync($categoriesIds);
            
            /* update offers */
            
            foreach ($productData['offers'] as $offerData) {
                $offer = Offer::firstOrNew(['id' => $offerData['id']]);
                
                $offer->fill(array_only($offerData, [
                    "price",
                    "amount",
                    "sales",
                    "article",
                ]));
                
                $offer->product_id = $product->id;
                $offer->save();
            }
        }
    }
}