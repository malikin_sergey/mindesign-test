<?php

namespace App;

/**
 * Class Product
 * @package App
 */
class Product extends \Eloquent
{
    /**
     * @type string
     */
    protected $table = "products";

    /**
     * @type array
     */
    protected $guarded = ['*'];
    
    /**
     * @type array
     */
    protected $casts = [];
    
    /**
     * @type bool
     */
    public $timestamps = false;
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function Categories()
    {
        return $this->belongsToMany(Category::class);
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Offers()
    {
        return $this->hasMany(Offer::class);
    }
    
    /**
     * @param $query
     */
    public function scopeAddSalesSum($query)
    {
        $query->selectRaw("(select sum(sales) from offers where offers.product_id = products.id) as sales_sum");
    }
    
    /**
     * @param $query
     */
    public function scopeAddAmountSum($query)
    {
        $query->selectRaw("(select sum(amount) from offers where offers.product_id = products.id) as amount_sum");
    }
}
