<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Offer
 * @package App
 */
class Offer extends \Eloquent
{
    /**
     * @type string
     */
    protected $table = "offers";
    
    /**
     * @type array
     */
    protected $guarded = ['*'];
    
    /**
     * @type array
     */
    protected $casts = [];
    
    /**
     * @type bool
     */
    public $timestamps = false;
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Product()
    {
        return $this->belongsTo(Product::class);
    }
}
